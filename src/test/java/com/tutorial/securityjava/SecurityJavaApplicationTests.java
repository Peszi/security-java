package com.tutorial.securityjava;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tutorial.securityjava.controller.BasicResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SecurityJavaApplicationTests {

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @Before
    public void setUp() { }

    @Test
    public void unprotectedEndpoint() throws Exception {

        final String expectedResponse = this.mapper
                .writeValueAsString(new BasicResponse("unprotected"));

        this.mvc.perform(get("/unprotected"))
                .andExpect(content().json(expectedResponse));
    }

    @Test
    public void protectedEndpoint() throws Exception {

        final String expectedResponse = this.mapper
                .writeValueAsString(new BasicResponse("protected"));

        this.mvc.perform(get("/protected"))
                .andExpect(status().isForbidden());
//                .andExpect(content().json(expectedResponse));
    }

}
