package com.tutorial.securityjava.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicRestController {

    @RequestMapping("/unprotected")
    public ResponseEntity<BasicResponse> getUnprotected() {
        return ResponseEntity.ok(new BasicResponse("unprotected"));
    }

    @RequestMapping("/unprotected/unprotected")
    public ResponseEntity<BasicResponse> getUnprotectedUnprotected(@AuthenticationPrincipal User user) {
        return ResponseEntity.ok(new BasicResponse("unprotected " + user.toString()));
    }

    @RequestMapping("/protected")
    public ResponseEntity<BasicResponse> getProtected() {
        return ResponseEntity.ok(new BasicResponse("protected"));
    }

    @RequestMapping("/protected/protected")
    public ResponseEntity<BasicResponse> getProtectedProtected() {
        return ResponseEntity.ok(new BasicResponse("protected"));
    }

}
