package com.tutorial.securityjava.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
//                .antMatcher("/unprotected/**")
                .authorizeRequests()
//                    .antMatchers("/login").permitAll()
                .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginProcessingUrl("/performLogin")
                    .defaultSuccessUrl("/homepage.html", true)
                .permitAll()
                    .and()
                .csrf().disable();
    }
}
